#! /usr/bin/env python3

import os
import random
from base64 import b64encode, b64decode

from Crypto.Cipher import AES

def local_file(name, mode="r"):
    current_dir = os.path.dirname(__file__)
    path = os.path.join(current_dir, name)
    return open(path, mode)
def file_bytes(name):
    with local_file(name, "rb") as f:
        return f.read()

# A convenience function that lets us use strings and bytes interchangeably.
def to_bytes(inpt):
    if isinstance(inpt, bytes) or isinstance(inpt, bytearray):
        return inpt
    elif isinstance(inpt, str):
        return inpt.encode("utf-8")
    else:
        raise RuntimeError("to_bytes can't convert " + str(type(inpt)))

## Part 9

# In order to avoid ambiguity, padding is always added. So if the input is a
# multiple of 16 bytes, an *additional* 16 bytes will be added (\x10\x10...).
def pad(b, n):
    b = to_bytes(b)
    r = n - (len(b) % n)
    return b + bytes([r]) * r

assert pad(b'12345678', 5) == b'12345678\x02\x02'
assert pad(b'1234', 4) == b'1234\x04\x04\x04\x04'

def unpad(b, n):
    val = b[-1]
    padding = b[-val:]
    rest = b[:-val]
    assert 0 < val <= n, "Padding value must be in range."
    assert len(b) % n == 0, "Input must be a multiple of the block size."
    assert padding == bytes([val] * val), "Padding must be all one char."
    return rest

assert b'abc' == unpad(b'abc\x02\x02', 5)
assert b'abc' == unpad(pad(b'abc', 10), 10)

## Part 10

def xor(buffer1, buffer2):
    return bytes(c1 ^ c2 for c1, c2 in zip(buffer1, buffer2))

def aes_encrypt_ecb(key, plain):
    aes = AES.new(to_bytes(key))
    return aes.encrypt(pad(plain, 16))

def aes_decrypt_ecb(key, cipher):
    aes = AES.new(to_bytes(key))
    return unpad(aes.decrypt(cipher), 16)

def aes_encrypt_cbc(key, plain):
    aes = AES.new(to_bytes(key))
    # IV of null bytes
    prev = bytes(16)
    out = bytearray()
    padded = pad(plain, 16)
    for i in range(len(padded) // 16):
        block = padded[16*i:16*(i+1)]
        xored_block = xor(block, prev)
        encrypted_block = aes.encrypt(xored_block)
        out.extend(encrypted_block)
        prev = encrypted_block
    return bytes(out)

def aes_decrypt_cbc(key, cipher):
    aes = AES.new(to_bytes(key))
    # IV of null bytes
    prev = bytes(16)
    out = bytearray()
    for i in range(len(cipher) // 16):
        block = cipher[16*i:16*(i+1)]
        decrypted_block = aes.decrypt(block)
        unxored_block = xor(decrypted_block, prev)
        out.extend(unxored_block)
        prev = block
    return unpad(bytes(out), 16)

# check that encrypt and decrypt are at least inverses of each other
key = b"YELLOW SUBMARINE"
test_input = b"foobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobar"
assert aes_decrypt_ecb(key, aes_encrypt_ecb(key, test_input)) == test_input
assert aes_decrypt_cbc(key, aes_encrypt_cbc(key, test_input)) == test_input

cipher_b64 = file_bytes("input_10.txt")
cipher = b64decode(cipher_b64)
plain = aes_decrypt_cbc(key, cipher)
# Check that the first few blocks are correct.
assert plain.startswith(b"I'm back and I'm ringin' the bell \nA rockin' on")
# Check that the last few blocks are correct, and unpad worked.
assert plain.endswith(b"Come on, Come on \nPlay that funky music \n")

## Part 11

def random_key():
    return os.urandom(16)

def oracle_11(plain, use_ecb):
    prefix = os.urandom(random.randint(5, 10))
    suffix = os.urandom(random.randint(5, 10))
    padded_plain = prefix + plain + suffix
    key = random_key()
    if use_ecb:
        cipher = aes_encrypt_ecb(key, padded_plain)
    else:
        cipher = aes_encrypt_cbc(key, padded_plain)
    return cipher

def detect_ecb(cipher_func):
    # Feed a large number of identical bytes into the cipher, so that even if
    # there's random padding at the front and back, there will be at least a
    # few identical 16 byte blocks in the middle, to give away the use of ECB.

    plain = bytes(100) # one hundred null bytes
    cipher = cipher_func(plain)
    chunks = set()
    for i in range(len(cipher) // 16):
        chunk = cipher[i*16:(i+1)*16]
        if chunk in chunks:
            return True
        chunks.add(chunk)
    return False

# Do a hundred random trials of detect_ecb()
for i in range(100):
    use_ecb = bool(random.randint(0,1))
    cipher_func = lambda plain: oracle_11(plain, use_ecb)
    use_ecb_test = detect_ecb(cipher_func)
    assert use_ecb == use_ecb_test, "detect_ecb() failed!"

## Part 12

def oracle_12_closure():
    # Make it (mostly) impossible for later code to read these secrets.
    key_12 = random_key()
    secret_message = b64decode(b'''
        Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkg
        aGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBq
        dXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUg
        YnkK''')
    def oracle_12(plain):
        return aes_encrypt_ecb(key_12, plain + secret_message)
    return oracle_12
oracle_12 = oracle_12_closure()

# Find the block size (pretending we don't already know it) by taking the
# difference between the shortest possible output and the second shortest.
# Also note the length of the secret message, based on the largest padding we
# can add to the front before the output size increases.
smallest_output_len = len(oracle_12(b""))
for i in range(100):
    next_len = len(oracle_12(b"a" * i))
    if next_len > smallest_output_len:
        block_size = next_len - smallest_output_len
        # Remember that right before the block size jumps, there is still one
        # byte of padding. There's never no padding.
        secret_len_12 = smallest_output_len - i
        break

# Make sure it equals 16, because that's what it is :p
assert block_size == 16

# Confirm that our detector indeed detects that oracle_12 is using ECB.
assert detect_ecb(oracle_12) == True

# For each byte in the secret message, decrypt it by putting it at the end of a
# 16 byte block whose first 15 bytes are known, either as previous bytes in the
# secret, or as a constant prefix of a's. Compute all 256 possible blocks, and
# use that map to figure out what the next byte is.
secret_message_12 = bytearray()
for i in range(secret_len_12):
    prefix = b"a" * (15 - len(secret_message_12)%16)
    candidates_map = {}
    for candidate_int in range(256):
        candidate_byte = bytes([candidate_int])
        candidate_plain = prefix + secret_message_12 + candidate_byte
        candidate_output = oracle_12(candidate_plain)[:len(candidate_plain)]
        candidates_map[candidate_output] = candidate_byte
    real_output = oracle_12(prefix)
    for candidate_output, candidate_byte in candidates_map.items():
        if real_output.startswith(candidate_output):
            secret_message_12 += candidate_byte
            break
    else:
        raise RuntimeError("None of the candidate outputs matched.")

assert secret_message_12.startswith(b"Rollin' in my 5.0\nWith my rag-top down")
assert secret_message_12.endswith(b"Did you stop? No, I just drove by\n")

## Part 13

def parse_query_params(params):
    params = to_bytes(params)
    pairs = params.split(b"&")
    split_pairs = [pair.split(b"=") for pair in pairs]
    return dict(split_pairs)

def clean_email(email):
    email = to_bytes(email)
    return email.replace(b"&", b"%26").replace(b"=", b"%3D")

def profile_for(email):
    return b"email=" + clean_email(email) + b"&uid=10&role=user"

assert parse_query_params(profile_for(b"a&=@b.com")) == {
        b"email" : b"a%26%3D@b.com",
        b"role" : b"user",
        b"uid" : b"10"}

# Secret! Shh!
key_13 = random_key()

def encrypt_profile(email):
    return aes_encrypt_ecb(key_13, profile_for(email))

def decrypt_profile(cipher):
    return parse_query_params(aes_decrypt_ecb(key_13, cipher))

assert decrypt_profile(encrypt_profile("a@b.com")) == \
        parse_query_params(profile_for("a@b.com"))

# To produce a "role=admin" encrypted profile, without knowing the password,
# first we need to get a profile where "role=" comes right before a block
# boundary. "email=&uid=10&role=" is 19 bytes, so we want an email address
# that's 32 - 19 = 13 bytes. We'll take the first two blocks of that.
prefix_blocks = encrypt_profile("a" * 13)[:32]

# Next, we need a block that contains just "admin", followed by the padding
# that encryption would've inserted if that were the last block. 16 - 5 = 11
# bytes, so we need 11 bytes of b"\x0b" padding. That needs to start at the
# beginning of a block, so we'll need another 10 bytes of padding in front, to
# sit after "email=" in the first block.
fancy_email = 10 * b"a" + b"admin" + b"\x0b" * 11
postfix_block = encrypt_profile(fancy_email)[16:32]

encrypted_admin_profile = prefix_blocks + postfix_block
admin_profile = decrypt_profile(encrypted_admin_profile)
assert admin_profile[b"role"] == b"admin"

## Part 14

# Reuse oracle_12, but prepend some consistent random bytes in front of the
# plaintext.
random_prefix_14 = os.urandom(random.randrange(20))
def oracle_14(plain):
    plain = to_bytes(plain)
    return oracle_12(random_prefix_14 + plain)

# First we need to find the length of the random prefix. Do this by increasing
# the length of our plaintext of just "\x00", until we see two identical
# blocks. Those must be filled up with "\x00". Then we know that the contents
# off all the blocks up until there, minus the length of our plaintext, must be
# the random prefix.
for null_text_len in range(100):
    cipher = oracle_14("\x00" * null_text_len)
    seen_blocks = set()
    found_repeat = False
    for i in range(len(cipher) // 16):
        block = cipher[i*16:(i+1)*16]
        if block in seen_blocks:
            found_repeat = True
            repeated_block_index = i
            break
        seen_blocks.add(block)
    if found_repeat:
        break
else:
    raise RuntimeError("Never found a repeat.")

random_prefix_len = (repeated_block_index + 1) * 16 - null_text_len

# Confirm the length we derived matches the actual random prefix.
assert random_prefix_len == len(random_prefix_14)

# As in part 12, find the length of the secret message by starting with an
# empty plaintext and increasing it until we see the first increase in the
# output size.
smallest_output_len = len(oracle_14(b""))
for i in range(100):
    next_len = len(oracle_14(b"\x00" * i))
    if next_len > smallest_output_len:
        # As above, no plus 1, because there's one byte of padding. And this
        # time we have to remember to subtract the random prefix length.
        secret_len_14 = smallest_output_len - i - random_prefix_len
        break

# Confirm with the real length of the secret message we decrypted in part 12.
assert secret_len_14 == secret_len_12

# Piece together the secret message just like we did in part 12. This time,
# treat the random prefix as though it's part of our own constant prefix, which
# is fine since it doesn't change.
secret_message_14 = bytearray()
for i in range(secret_len_14):
    infix_len = 15 - (random_prefix_len + len(secret_message_14))%16
    infix = b"a" * infix_len
    candidates_map = {}
    for candidate_int in range(256):
        candidate_byte = bytes([candidate_int])
        candidate_plain = infix + secret_message_14 + candidate_byte
        candidate_output_len = len(candidate_plain) + random_prefix_len
        candidate_output = oracle_14(candidate_plain)[:candidate_output_len]
        candidates_map[candidate_output] = candidate_byte
    real_output = oracle_14(infix)
    for candidate_output, candidate_byte in candidates_map.items():
        if real_output.startswith(candidate_output):
            secret_message_14 += candidate_byte
            break
    else:
        raise RuntimeError("None of the candidate outputs matched.")

# Make sure we got the same message that we did in 12.
assert secret_message_14 == secret_message_12

## Part 15
# See the unpad() function above.

## Part 16
key_16 = random_key()

def take_input(input_str):
    clean_str = input_str.replace(";", "%3B").replace("=", "%3D")
    full_input = ("comment1=cooking%20MCs;userdata=" +
            clean_str + ";comment2=%20like%20a%20pound%20of%20bacon")
    return aes_encrypt_cbc(key_16, full_input)

def is_admin(encrypted_input):
    return b";admin=true;" in aes_decrypt_cbc(key_16, encrypted_input)

# Make sure escaping works properly. We can't just inject the payload we want.
assert not is_admin(take_input(";admin=true;"))

# In CBC, each block is xor'd with the preceding encrypted block after it's
# decrypted. Modifying an encrypted block will cause it to decrypt to junk, but
# if we don't mind that, and if the contents of the following block are known,
# we can edit the following block. We do this by xor'ing the following block's
# contents and the current encrypted block, with the new contet that we would
# like. This cancels out the original content and the xor'ing done by CBC,
# leaving only the new bytes that we wanted.
empty_output = take_input("")
magic_chunk = xor(xor(b"%20MCs;userd", b";admin=true;"), empty_output[:12])
# Replace the first 12 bytes of the original output with our magic_chunk. The
# first block will turn into garbage, but that's fine, because all we care
# about is that the second block now contains the string ";admin=true;" when
# the whole thing is decrypted.
modified_output = magic_chunk + empty_output[len(magic_chunk):]

# Confirm that we've hacked in the string we wanted.
assert is_admin(modified_output)
