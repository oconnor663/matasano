#! /usr/bin/python3

# PyCrypto
from Crypto.Cipher import AES

from binascii import hexlify, unhexlify
from base64 import b64encode, b64decode
import os
import string
import math
import itertools

def local_file(name, mode="r"):
    current_dir = os.path.dirname(__file__)
    path = os.path.join(current_dir, name)
    return open(path, mode)
def file_lines(name):
    with local_file(name) as f:
        return (line.strip() for line in f.readlines())
def file_bytes(name):
    with local_file(name, "rb") as f:
        return f.read()

## Part 1
hex_in = b"49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d"
data = unhexlify(hex_in)
b64_out = b64encode(data)
assert b64_out == b"SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t"
assert hexlify(b64decode(b64_out)) == hex_in

## Part 2
def xor(buffer1, buffer2):
    return bytes(c1 ^ c2 for c1, c2 in zip(buffer1, buffer2))

data1 = unhexlify("1c0111001f010100061a024b53535009181c")
data2 = unhexlify("686974207468652062756c6c277320657965")
data_out = xor(data1, data2)
assert hexlify(data_out) == b"746865206b696420646f6e277420706c6179"

## Part 3
def char_vector(b):
    vector = [0] * 256
    for c in b:
        if 0 <= c <= 255:
            vector[c] += 1
    return tuple(vector)
english_vector = char_vector(file_bytes("english_corpus.txt"))

magnitude_cache = {}
def magnitude(v):
    if v in magnitude_cache:
        return magnitude_cache[v]
    ret = math.sqrt(sum(i**2 for i in v))
    magnitude_cache[v] = ret
    return ret
def normalized_dot(v1, v2):
    dot =  sum(x1 * x2 for x1, x2 in zip(v1, v2))
    magnitude1 = magnitude(v1)
    magnitude2 = magnitude(v2)
    if magnitude1 == 0 or magnitude2 == 0:
        return 0
    return dot / magnitude1 / magnitude2
def score_bytes(b, reference_vector):
    return normalized_dot(reference_vector, char_vector(b))
def antixor(b, reference_vector):
    max_score = 0
    for x in range(0, 256):
        new_b = bytes(c^x for c in b)
        score = score_bytes(new_b, reference_vector)
        if score > max_score:
            max_score = score
            key = x
            text = new_b
    return (key, text)

data = unhexlify("1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736")
assert antixor(data, english_vector) == (ord('X'), b"Cooking MC's like a pound of bacon")

## Part 4
# We could solve this by antixor'ing every line, but antixor is slow, so we do
# a shortcut to detect the best line first. Get the character frequency of the
# whole file, and look for the one that doesn't match, on the assumption that
# that one came from English and the rest are random.
lines = file_lines("input_4.txt")
decoded_lines = [ unhexlify(line) for line in lines ]
vector_4 = char_vector(itertools.chain.from_iterable(decoded_lines))
best_line = min(decoded_lines, key = lambda line: score_bytes(line, vector_4))
# Now just antixor that one line.
key, text = antixor(best_line, english_vector)
assert text == b"Now that the party is jumping\n"

## Part 5
def repeating_xor(key, b):
    return bytes(b[i] ^ key[i%len(key)] for i in range(len(b)))

mad_rhymes = b'''\
Burning 'em, if you ain't quick and nimble
I go crazy when I hear a cymbal'''
encoded_rhymes = (
        b"0b3637272a2b2e63622c2e69692a23693a2a3c6324202d623d" +
        b"63343c2a26226324272765272a282b2f20430a652e2c652a3" +
        b"124333a653e2b2027630c692b20283165286326302e27282f")
assert hexlify(repeating_xor(b"ICE", mad_rhymes)) == encoded_rhymes

## Part 6
def hamming_distance(s1, s2):
    def count_bits(c):
        bits = 0
        while c != 0:
            bits += c % 2
            c >>= 1
        return bits
    assert len(s1) == len(s2)
    return sum(count_bits(c1 ^ c2) for c1, c2 in zip(s1, s2))
assert hamming_distance(b"this is a test", b"wokka wokka!!!") == 37

base64_input = file_bytes("input_6.txt")
input_bytes = b64decode(base64_input)

# Get the best keysize by looking at edit distances between sample blocks of
# that keysize.
min_distance = None
best_keysize = None
for keysize in range(2, 40):
    distances = []
    # Take the first 10 blocks of that keysize and average the edit distance
    # between each adjacent pair. In this problem, it turns out that the
    # absolute minimum number of comparisons we need to do before the right
    # answer emerges is 9.
    for i in range(10):
        block1 = input_bytes[i*keysize:(i+1)*keysize]
        block2 = input_bytes[(i+1)*keysize:(i+2)*keysize]
        distances.append(hamming_distance(block1, block2) / keysize)
    mean_distance = sum(distances) / len(distances)
    if min_distance == None or mean_distance < min_distance:
        min_distance = mean_distance
        best_keysize = keysize

# Now solve each byte of the key independently
key_bytes = []
for key_index in range(best_keysize):
    input_slice = input_bytes[key_index::best_keysize]
    key, text = antixor(input_slice, english_vector)
    key_bytes.append(key)
solved_key = bytes(key_bytes)
solved_text = repeating_xor(solved_key, input_bytes)
assert solved_text.startswith(b"I'm back and I'm ringin' the bell")

## Part 7
aes = AES.new("YELLOW SUBMARINE")
ciphertext = b64decode(file_bytes("input_7.txt"))
text = aes.decrypt(ciphertext)
assert text.startswith(b"I'm back and I'm ringin' the bell")

## Part 8
input_lines = [unhexlify(line) for line in file_lines("input_8.txt")]
ecb_line = None
ecb_chunk = None
chunk_len = 16
# Break the lines up into chunks of 16 bytes, and just look for chunks that get
# repeated. The first line with a repeat is assumed to be the ECB line.
for line in input_lines:
    chunks = set()
    num_chunks = len(line) // chunk_len
    for i in range(num_chunks):
        chunk = line[i*chunk_len:(i+1)*chunk_len]
        if chunk in chunks:
            ecb_line = line
            ecb_chunk = chunk
            break
        chunks.add(chunk)
    if ecb_line:
        break

answer_chunk = unhexlify(b"08649af70dc06f4fd5d2d69c744cd283")
assert ecb_line.count(answer_chunk) == 4
